-- Minetest 0.4 mod: bones
-- See README.txt for licensing and other information. 

minetest.override_item("bones:bones", {
	
	on_metadata_inventory_take = function(pos, listname, index, stack, player)
		local meta = minetest.get_meta(pos)
		if meta:get_string("owner") ~= "" and meta:get_inventory():is_empty("main") then
			meta:set_string("infotext", meta:get_string("owner").."'s old bones")
			meta:set_string("formspec", "")
			meta:set_string("owner", "")
		end
	end,

	on_punch = minetest.node_punch
})
